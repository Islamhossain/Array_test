<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // array 1.. indexed..
        echo "<u>indexed Array example 1:</u><br/>";
        $array1 = array(1,2,3);
        echo "<pre>";
        echo var_dump($array1); 
        
        //array 2 indexed..
        echo "<u>indexed array example 2:</u></br>";
        $array2 = array(1,"dhaka","bangladesh");
        echo "<pre>";
        echo var_dump($array2); 
        echo "</pre>";
        
        // array 3 indexed..
        echo "<u>indexed Array example 3:</u><br/>";
        $array3 = array(1=>"name",2=>"roll",3=>"bangladesh");
        echo "<pre>";
        echo var_dump($array3);
        echo "</pre>";
        
        echo "<h1>two dimentional array</h1>";
        $array_m_1  = array(
                                          "sat"=>"dhanmondi",
                                           "sun"=>array(
                                                                            "jadu",
                                                                            "kodu",
                                                                            "modhu"
                                                                   ),
                                            "mon"=>"bonani"
            );
        
        echo $array_m_1["sat"]; 
                echo "<br/>".$array_m_1["sun"][0]; 
                echo "<br/>".$array_m_1["sun"][1]; 
                echo "<br/>".$array_m_1["sun"][2];
        
        
        ?>        
    </body>
</html>
